package com.mycompany.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table
public class Musicas implements Serializable {

	@Transient
	@JsonIgnore()
	private static final long serialVersionUID = 2093174280843154562L;

	@Id
	@JsonProperty
	private String Id;

	@JsonProperty
	private String ArtistaId;

	@JsonProperty
	private String Nome;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getArtistaId() {
		return ArtistaId;
	}

	public void setArtistaId(String artistaId) {
		ArtistaId = artistaId;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

}
