package com.mycompany.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table
public class PlaylistMusicas implements Serializable {

	@Transient
	@JsonIgnore()
	private static final long serialVersionUID = -2039985953338962184L;

	@Id
	@JsonProperty
	private String PlaylistId;

	@Id
	@JsonProperty
	private String MusicaId;

	public String getPlaylistId() {
		return PlaylistId;
	}

	public void setPlaylistId(String playlistId) {
		PlaylistId = playlistId;
	}

	public String getMusicaId() {
		return MusicaId;
	}

	public void setMusicaId(String musicaId) {
		MusicaId = musicaId;
	}

}
