package com.mycompany.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table
public class Usuarios implements Serializable {

	@Transient
	@JsonIgnore()
	private static final long serialVersionUID = 6789315915505947077L;

	@Id
	@JsonProperty
	private String Id;

	@JsonProperty
	private String Nome;

	@JsonProperty
	private String PlaylistId;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getPlaylistId() {
		return PlaylistId;
	}

	public void setPlaylistId(String playlistId) {
		PlaylistId = playlistId;
	}

}
