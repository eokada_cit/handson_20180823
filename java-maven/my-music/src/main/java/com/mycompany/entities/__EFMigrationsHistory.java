package com.mycompany.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table
public class __EFMigrationsHistory implements Serializable {

	@Transient
	@JsonIgnore()
	private static final long serialVersionUID = 3304525337830436852L;

	@Id
	@JsonProperty
	private String MigrationId;

	@JsonProperty
	private String ProductVersion;

	public String getMigrationId() {
		return MigrationId;
	}

	public void setMigrationId(String migrationId) {
		MigrationId = migrationId;
	}

	public String getProductVersion() {
		return ProductVersion;
	}

	public void setProductVersion(String productVersion) {
		ProductVersion = productVersion;
	}

}
