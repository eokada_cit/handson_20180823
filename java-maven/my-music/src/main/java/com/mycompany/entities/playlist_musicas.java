package com.mycompany.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table
public class playlist_musicas implements Serializable {

	@Transient
	@JsonIgnore()
	private static final long serialVersionUID = 1L;

	@Id
	@JsonProperty
	private String playlistid;

	@Id
	@JsonProperty
	private String musicaid;

	public String getPlaylistid() {
		return playlistid;
	}

	public void setPlaylistid(String playlistid) {
		this.playlistid = playlistid;
	}

	public String getMusicaid() {
		return musicaid;
	}

	public void setMusicaid(String musicaid) {
		this.musicaid = musicaid;
	}

}
